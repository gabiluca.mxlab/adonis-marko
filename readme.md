
# Adonis MarkoJs

[MarkoJs](https://gitlab.com/gabiluca.mx.gitlab/adonis-marko.git) templating provider for AdonisJs framework version 4.

## Installation

In order to use adonis-marko

```
npm install https://gitlab.com/gabiluca.mx.gitlab/adonis-marko.git --save
```

Once you have installed the provider make sure that the ViewProvider is registered as a provider inside start/app.js file.

```javascript
const providers = [
  'adonis-marko/providers/ViewProvider'
]
```

Make sure the default edge provider (`@adonisjs/framework/providers/ViewProvider`) is not registered as they will conflict with each other.

See the [MarkoJs API documentation](https://markojs.com) for more info on these options.

## Basic Usage

Let’s start with the basic example of saying `Hello world` by rendering a marko template. All of the views are stored inside `resources/views` directory and end with `.marko` extension.

Create a marko template at `resources/views/hello.marko`. You can use an adonis/ace command to create the view.

```sh
adonis make:marko home

    ✔ create  resources/views/home.marko
```

Now let's create a route that renders it:

```javascript
Route.get('/', ({ view }) => {
  return view.render('home')
})
```

The view.render method takes the relative path to the view file. There is no need to type .marko extension.


## View Methods

These methods are available on the view context object in controllers and middleware.

#### view.share(locals)
Share variables as a local with this template context.


| Param | Type | Description |
| --- | --- | --- |
| locals | <code>Object</code> | Key value pairs |

###### *Example*

Quite often you want to share request specific values with your views, this can be done in middleware or controllers by passing an object to the share method.

```javascript
class SomeMiddleware {
  async handle ({ view }, next) {
    view.share({
      apiVersion: request.input('version')
    })

    await next()
  }
}
```

Inside your views, you can access it like any other variable

```marko
p= apiVersion
```

#### view.render(template, locals) ⇒ <code>String</code>
Render a marko template

**Returns**: <code>String</code> - HTML rendered output  

| Param | Type | Description |
| --- | --- | --- |
| template | <code>String</code> | View file (.marko extension not required) |
| locals | <code>Object</code> | Variables to be passed to the view |

#### view.renderString(string, locals) ⇒ <code>String</code>
Render a string of marko

**Returns**: <code>String</code> - HTML rendered output  

| Param | Type | Description |
| --- | --- | --- |
| string | <code>String</code> | String to be rendered |
| locals | <code>Object</code> | Variables to be passed to the view |


## View Helpers

A number of global methods and contextual helpers are injected into all views.

### Request

All views have access to the current request object, and you can call request methods inside your templates as well.

```marko
p The request URL is ${request.url()}
```

Also, there is a direct helper to get the URL.

```marko
p The request URL is ${url}
```

### style - *formerly css (deprecated)*

Add link tag to a CSS file. The file name should be relative from the public directory. Absolute links are left alone (for external CDNs etc)

``` marko
$!{style('style')}
// Renders <link rel='stylesheet' href="/style.css">
```

### script

Similar to css, adds a script tag to the document

``` marko
$!{script('my-script')}
// Renders <script type="text/javascript" src="/my-script.js"></script>'
```

### auth
If you are using the auth provider, then you can access the current logged in user using the `auth.user` object.

### csrfField
If you are using the shield middleware, you can access the `csrfToken` and field using one of the following methods.

```marko
$!{csrfField()}
// Renders <input type="hidden" name="_csrf" value="..." />
```

### cspMeta

When using shield middleware, the CSP headers are set automatically. However can also set them using HTML meta tags.

```marko
<head>
  $!{cspMeta()}
</head>
```

## Extending views

You can also extend views by adding your own view globals. Globals should only be added once, so make sure to use the start/hooks.js file and add them using the after providersBooted hook.

### Globals

``` javascript
const { hooks } = require('@adonisjs/ignitor')

hooks.after.providersBooted(() => {
  const View = use('View')

  View.global('currentTime', function () {
    return new Date().getTime()
  })
})
```

Above global returns the current time when you reference it inside the views.

```marko
 The time is ${currentTime()}
```

You can extract the code inside providersBooted to a different file and require it.

### Globals scope

The value of `this` inside globals closure is bound to the template context so that you can access runtime values from it.

To use other global methods or values, make use of the this.globals object.

```javascript
View.global('messages', {
  success: 'This is a success message',
  warning: 'This is a warning message'
})

View.global('getMessage', function (type) {
  return this.globals.messages[type]
})
```
